﻿using System;
using System.Collections.Generic;

namespace EFDbFirst.Model
{
    public partial class Athlete
    {
        public Athlete()
        {
            Coach = new HashSet<Coach>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public DateTime? Dob { get; set; }

        public virtual ICollection<Coach> Coach { get; set; }
    }
}
