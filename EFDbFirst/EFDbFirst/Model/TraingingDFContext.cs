﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace EFDbFirst.Model
{
    public partial class TraingingDFContext : DbContext
    {
        public TraingingDFContext()
        {
        }

        public TraingingDFContext(DbContextOptions<TraingingDFContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Athlete> Athlete { get; set; }
        public virtual DbSet<Coach> Coach { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source= MININT-07CHMFP\\SQLEXPRESS; Initial Catalog=TraingingDF; Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Athlete>(entity =>
            {
                entity.Property(e => e.Dob).HasColumnName("DOB");

                entity.Property(e => e.Gender).HasMaxLength(20);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Coach>(entity =>
            {
                entity.Property(e => e.Gender).IsRequired();

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.HasOne(d => d.Athlete)
                    .WithMany(p => p.Coach)
                    .HasForeignKey(d => d.AthleteId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Coach_Athlete");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
