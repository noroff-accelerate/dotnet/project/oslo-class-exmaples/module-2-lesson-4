﻿using System;
using System.Collections.Generic;

namespace EFDbFirst.Model
{
    public partial class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public int AthleteId { get; set; }

        public virtual Athlete Athlete { get; set; }
    }
}
