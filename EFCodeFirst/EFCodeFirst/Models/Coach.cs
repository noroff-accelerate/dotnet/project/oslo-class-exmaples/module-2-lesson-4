﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EFCodeFirst.Models
{
    class Coach
    {
        // PK
        public int Id { get; set; }

        // Fields
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public int Awards { get; set; }

        ICollection<CoachCert> CoachCerts { get; set; }

    }
}
