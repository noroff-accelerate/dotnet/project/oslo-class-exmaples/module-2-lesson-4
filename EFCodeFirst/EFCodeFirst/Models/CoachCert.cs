﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCodeFirst.Models
{
    class CoachCert
    {
        public int CoachId { get; set; }
        public int CertId { get; set; }

        // Nav
        public Coach Coach { get; set; }
        public Cert Cert { get; set; }
    }
}
