﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EFCodeFirst.Models
{
    class TrainingDbContext : DbContext
    {
        // What tables to add
        public DbSet<Coach> Coaches { get; set; }
        public DbSet<Athlete> Athletes { get; set; }

        public DbSet<Cert> Certs { get; set; }

        public DbSet<CoachCert> CoachCerts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source= MININT-07CHMFP\\SQLEXPRESS; Initial Catalog=Training; Integrated Security=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // COmposite key
            modelBuilder.Entity<CoachCert>().HasKey(cc => new { cc.CertId, cc.CoachId });
            // Seed some data
            modelBuilder.Entity<Cert>().HasData(new Cert { Id=1, Name="Boxing" });
            modelBuilder.Entity<Cert>().HasData(new Cert { Id=2, Name="Running" });
            modelBuilder.Entity<Cert>().HasData(new Cert { Id=3, Name="Badass" });
        }
       
    }
}
