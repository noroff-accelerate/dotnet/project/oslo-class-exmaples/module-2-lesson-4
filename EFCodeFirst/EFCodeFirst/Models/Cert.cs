﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCodeFirst.Models
{
    class Cert
    {
        public int Id { get; set; }
        public string Name { get; set; }

        ICollection<CoachCert> CoachCerts { get; set; }
    }
}
