﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFCodeFirst.Models
{
    class Athlete
    {
        // PK
        public int Id { get; set; }

        // Fields
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }
        public int Records { get; set; }
        // Add relationship
        public int CoachId { get; set; }
        public Coach Coach { get; set; }
    }
}
