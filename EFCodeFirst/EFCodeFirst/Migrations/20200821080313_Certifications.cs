﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class Certifications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cert",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cert", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CoachCert",
                columns: table => new
                {
                    CoachId = table.Column<int>(nullable: false),
                    CertId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoachCert", x => new { x.CertId, x.CoachId });
                    table.ForeignKey(
                        name: "FK_CoachCert_Cert_CertId",
                        column: x => x.CertId,
                        principalTable: "Cert",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoachCert_Coach_CoachId",
                        column: x => x.CoachId,
                        principalTable: "Coach",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cert",
                columns: new[] { "Id", "Name" },
                values: new object[] { 1, "Boxing" });

            migrationBuilder.InsertData(
                table: "Cert",
                columns: new[] { "Id", "Name" },
                values: new object[] { 2, "Running" });

            migrationBuilder.InsertData(
                table: "Cert",
                columns: new[] { "Id", "Name" },
                values: new object[] { 3, "Badass" });

            migrationBuilder.CreateIndex(
                name: "IX_CoachCert_CoachId",
                table: "CoachCert",
                column: "CoachId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoachCert");

            migrationBuilder.DropTable(
                name: "Cert");
        }
    }
}
