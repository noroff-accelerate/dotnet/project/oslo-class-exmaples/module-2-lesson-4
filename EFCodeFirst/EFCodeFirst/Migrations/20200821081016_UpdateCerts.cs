﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EFCodeFirst.Migrations
{
    public partial class UpdateCerts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Athlete_Coach_CoachId",
                table: "Athlete");

            migrationBuilder.DropForeignKey(
                name: "FK_CoachCert_Cert_CertId",
                table: "CoachCert");

            migrationBuilder.DropForeignKey(
                name: "FK_CoachCert_Coach_CoachId",
                table: "CoachCert");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CoachCert",
                table: "CoachCert");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Coach",
                table: "Coach");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Cert",
                table: "Cert");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Athlete",
                table: "Athlete");

            migrationBuilder.RenameTable(
                name: "CoachCert",
                newName: "CoachCerts");

            migrationBuilder.RenameTable(
                name: "Coach",
                newName: "Coaches");

            migrationBuilder.RenameTable(
                name: "Cert",
                newName: "Certs");

            migrationBuilder.RenameTable(
                name: "Athlete",
                newName: "Athletes");

            migrationBuilder.RenameIndex(
                name: "IX_CoachCert_CoachId",
                table: "CoachCerts",
                newName: "IX_CoachCerts_CoachId");

            migrationBuilder.RenameIndex(
                name: "IX_Athlete_CoachId",
                table: "Athletes",
                newName: "IX_Athletes_CoachId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CoachCerts",
                table: "CoachCerts",
                columns: new[] { "CertId", "CoachId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Coaches",
                table: "Coaches",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Certs",
                table: "Certs",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Athletes",
                table: "Athletes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Athletes_Coaches_CoachId",
                table: "Athletes",
                column: "CoachId",
                principalTable: "Coaches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CoachCerts_Certs_CertId",
                table: "CoachCerts",
                column: "CertId",
                principalTable: "Certs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CoachCerts_Coaches_CoachId",
                table: "CoachCerts",
                column: "CoachId",
                principalTable: "Coaches",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Athletes_Coaches_CoachId",
                table: "Athletes");

            migrationBuilder.DropForeignKey(
                name: "FK_CoachCerts_Certs_CertId",
                table: "CoachCerts");

            migrationBuilder.DropForeignKey(
                name: "FK_CoachCerts_Coaches_CoachId",
                table: "CoachCerts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Coaches",
                table: "Coaches");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CoachCerts",
                table: "CoachCerts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Certs",
                table: "Certs");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Athletes",
                table: "Athletes");

            migrationBuilder.RenameTable(
                name: "Coaches",
                newName: "Coach");

            migrationBuilder.RenameTable(
                name: "CoachCerts",
                newName: "CoachCert");

            migrationBuilder.RenameTable(
                name: "Certs",
                newName: "Cert");

            migrationBuilder.RenameTable(
                name: "Athletes",
                newName: "Athlete");

            migrationBuilder.RenameIndex(
                name: "IX_CoachCerts_CoachId",
                table: "CoachCert",
                newName: "IX_CoachCert_CoachId");

            migrationBuilder.RenameIndex(
                name: "IX_Athletes_CoachId",
                table: "Athlete",
                newName: "IX_Athlete_CoachId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Coach",
                table: "Coach",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CoachCert",
                table: "CoachCert",
                columns: new[] { "CertId", "CoachId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_Cert",
                table: "Cert",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Athlete",
                table: "Athlete",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Athlete_Coach_CoachId",
                table: "Athlete",
                column: "CoachId",
                principalTable: "Coach",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CoachCert_Cert_CertId",
                table: "CoachCert",
                column: "CertId",
                principalTable: "Cert",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CoachCert_Coach_CoachId",
                table: "CoachCert",
                column: "CoachId",
                principalTable: "Coach",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
